#!/bin/bash

export PATH=./node_modules/.bin:$PATH

bash build.sh

cd collabthings-app
electron-builder install-app-deps
electron-builder -p always .

