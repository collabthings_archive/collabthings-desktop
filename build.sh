#!/bin/bash

echo BUILD collabthings-desktop $(date)

export PATH=./node_modules/.bin:$PATH

c=$(pwd)
cd $c/collabthings-api

if npm run buildtest ; then
	echo Api build ok

	echo Building docs	
	cd $c/collabthings-app-docs
	bash buildtest.sh

	echo Running app buildtest
	cd $c/collabthings-app
	bash build.sh
fi

